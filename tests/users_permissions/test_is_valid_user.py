import pytest
from users.models import CustomUser, UniversityUser
from utils.user.user_type_util import UserType

def test_user_type_validation():
    user_types = ['university_tester', 'super_user', 'university_user']
    expected_error_messages = [
        f'User type ({user_types[0]}) does not exist',
        f"Wrong User type ({user_types[1]}) for this Model User (<class 'users.models.UniversityUser'>)",
        f"Wrong User type ({user_types[2]}) for this Model User (<class 'users.models.CustomUser'>)"
    ]
    user_models = [UniversityUser, UniversityUser, CustomUser]

    for i in range(3):
        with pytest.raises(Exception) as context:
            UserType.is_valid_user_type(user_types[i], user_model=user_models[i])

        actual_error_message = str(context.value)
        assert actual_error_message == expected_error_messages[i], f"Esperado: {expected_error_messages[i]}, Obtido: {actual_error_message}"